import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [conference, setConference] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [conferences, setConferences] = useState([]);
    const [synopsis, setSynopsis] = useState('');
    const [title, setTitle] = useState('');

    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.conference = conference;
    data.presenter_name = name;
    data.presenter_email = email;
    data.company_name = companyName;
    data.synopsis = synopsis;
    data.title = title;

    console.log(data)

    const url = `http://localhost:8000${conference}presentations/`;
    const fetchConfig = {
        method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
    setConference('');
    setName('');
    setEmail('');
    setSynopsis('');
    setTitle('');    }
  }
  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const handleChangeEmail = (event) => {
    const value = event.target.value;
    setEmail(value);
  }

  const handleChangeConference = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleChangeTitle = (event) => {
    const value = event.target.value;
    setTitle(value);
  }

  const handleChangeSynopsis = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }

  const handleChangeCompanyName = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
              <input onChange={handleChangeName} placeholder="Name" required type="text" id="presenter_name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeEmail} placeholder="Email" required type="email" id="presenter_email" className="form-control" />
              <label htmlFor="email">Presenter's Email</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeCompanyName} placeholder="Company Name" required type="text" id="company_name" className="form-control" />
              <label htmlFor="companyName">Company Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeTitle} placeholder="Presentation Title" required type="text" className="form-control" />
              <label htmlFor="title">Presentation Title</label>
            </div>


            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis
              <textarea onChange={handleChangeSynopsis} className="form-control" name="synopsis" id="synopsis" cols={60} rows={3}></textarea>
              </label>
            </div>

            <div className="mb-3">
              <select onChange={handleChangeConference} required id="conference" name="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default PresentationForm;
